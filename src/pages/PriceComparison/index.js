import React, {useState} from 'react';
import {
  AsyncStorage,
  FlatList,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  View
} 
from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Header, Icon } from 'react-native-elements';
import { useFocusEffect } from '@react-navigation/native';
import { getOdoo, connectAPI } from '../../utils/odoo.js';
import { TextInputMask } from 'react-native-masked-text';
import { 
  selectBy, 
  insertData, 
  update,
  selectAll,
  selectByMultiple } 
from '../../database/database.js';

import styles from './styles.js';

export default function PriceComparison(props) {
  const [product, setProduct] = useState([]); 
  const [task, setTask] = useState(0);
  const [price, setPrice] = useState('');

  useFocusEffect(
    React.useCallback(() => {
      AsyncStorage.getItem('challenge_id').then((r)=>{
        console.log('challenge_id');

        setTask(JSON.parse(r).key);

        AsyncStorage.getItem('user').then((res)=>{
          const user = JSON.parse(res);

          const odoo = getOdoo(user.email, user.password);
          connectAPI(odoo).then((r) => {
            const params = {
              domain: [['id', '=', task]],
            }

            odoo.search_read('pops.price_comparison', params).then(function(res){
              if(res.success){
                console.log(res.data[0]["product_id"]);
                console.log(res.data[0]["product_id"][0]);
                console.log(res.data[0]["product_id"][1]);
                const p_ = {
                  id: res.data[0]["product_id"][0],
                  name: res.data[0]["product_id"][1]
                };
                setProduct(p_);
              }
            })
          })
        });
      })
    }, [])
  );


  return (

    <SafeAreaView 
      style={[styles.white, { flexGrow: 1, marginTop:Platform.OS === 'ios' ? 0 : -50 }]} >
      <Header
        backgroundColor='#fff'
        leftComponent={{ 
          icon: 'keyboard-arrow-left', 
          size: 36,
          color: '#333',
          onPress: () => {
            // setIsLoading(true);
            props.navigation.navigate('MissionProgress');
          }
        }}
        containerStyle={{ 
          paddingTop: 50, 
        }}
      />
      <View style={{flex: 1, flexGrow: 1, backgroundColor: '#fff', justifyContent: 'space-between'}}>
        <View style={{flexGrow: 1}}>
          <Text style={{
            textAlign:'center', 
            fontSize: 22}}>Produto</Text>
          <Text style={{
            textAlign:'center', 
            fontSize: 22, 
            paddingVertical: 40}}>{product.name}</Text>
        </View>
        <View style={{flexGrow: 1}}>
          <Text style={{
            textAlign:'center', 
            fontSize: 22}}>Preço</Text>
          <TextInputMask
            type={'money'}
            options={{
              precision: 2,
              separator: ',',
              delimiter: '.',
              unit: 'R$',
              suffixUnit: ''
            }}
            style={{
              borderWidth: 1, 
              fontSize: 18, 
              width: 200, 
              height: 40,
              borderRadius: 10,
              textAlign: 'center',
              marginTop: 10,
              marginBottom: 10,
              marginRight: 'auto', 
              marginLeft: 'auto'}}
            value={price}
            onChangeText={text => {
              console.log(text);
              setPrice(text)
            }}
          />
        </View>
        <View>
        <TouchableHighlight
            style={styles.button}
            onPress={() => {
              salvar()
            }}
            underlayColor="#fff">
            <Text style={styles.submitText}>Salvar</Text>
          </TouchableHighlight>
        </View>
      </View>
    </SafeAreaView>
  )


}