import React from 'react';
// import InputScrollView from 'react-native-input-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';
import {Header} from 'react-native-elements';
// import Constants from 'expo-constants';
import { WebView } from 'react-native-webview';


// import {
//   Dimensions
// } from 'react-native';

export default function Termos(props) {
  console.log(props);

  const params = props.params;



  return (
    <SafeAreaView style={{flex: 1}}>
      <Header
        backgroundColor='#e40063'
        leftComponent={{ 
          icon: 'arrow-back', 
          size: 32,
          color: '#fff',
          onPress: () => props.navigation.navigate('Config')
        }}
        containerStyle={{
          borderBottomWidth: 0, 
          paddingTop: 0, 
          height: 60
        }}
      />
      <WebView source={{ uri: 'http://goop.popsolutions.co/termos-de-uso' }} style={{ marginTop: -60}}/>
    </SafeAreaView>
  );
}