import React, { useEffect, useState } from 'react';

import * as yup from 'yup';
import { Formik } from 'formik';
import Swiper from 'react-native-swiper';
import { TextInputMask } from 'react-native-masked-text';
import ModalSelector from 'react-native-modal-selector';
import { CheckBox, Icon, Overlay } from 'react-native-elements';

import { SafeAreaView } from 'react-native-safe-area-context';
import { useFocusEffect } from '@react-navigation/native';
import InputScrollView from 'react-native-input-scroll-view';
import AnimatedLoader from "react-native-animated-loader";

import Moment from 'moment';
import { Camera } from 'expo-camera';
import { getOdoo, connectAPI } from '../../utils/odoo.js';
import { WebView } from 'react-native-webview';

import {
  AsyncStorage,
  BackHandler,
  Button,
  Dimensions,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Alert
} from 'react-native';

import styles from './styles.js';

export default function Cadastro(props) {
  const [foto, setFoto] = useState('');

  const [address, setAddress] = useState({});
  const [overlayVisible, setOverlayVisible] = useState(false);
  const [message, setMessage] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [aceito, setAceito] = useState(false);
  const [cpfvalido, setCPFvalido] = useState(false);
  const [genero, setGenero] = useState('');
  const [editablegender, setEditableGender] = useState(true);
  const [hasPermission, setHasPermission] = useState(null);
  const [loaderVisible, setLoaderVisible] = useState(false);

  const [user, setUser] = useState({});

  const [swipeAllowed, setSwipeAllowed] = useState(false);
  const [showPagination, setShowPagination] = useState(true);

  const [confirmEmail, setConfirmEmail] = useState(false);


  const inputs = {};
  let swipper = '';

  let { height, width } = Dimensions.get('window');

  const data = [
    { key: 0, label: 'Feminino' },
    { key: 1, label: 'Masculino' },
  ];

  const initialValues = {
    image: '',
    name: '',
    cnpj_cpf: '',
    birthdate: '',
    gender: '',
    email: '',
    mobile: ''
  };

  const initialValues2 = {
    zip: '',
    education_level: '',
    function: '',
    accept: aceito
  };

  function focusNextField(id) {
    try {
      if (inputs[id].getElement) {
        inputs[id].getElement().focus();
      } else {
        inputs[id].focus();
      }
    } catch (e) {
      console.log(e);
    }
  }

  async function storeData(name, object) {
    AsyncStorage.mergeItem(name, JSON.stringify(object));
    let b = await getItemStored(name);
    return b;
  }

  async function getItemStored(name) {
    let r = await AsyncStorage.getItem(name, function (res) {
      return res;
    });
    return r;
  }

  function submit(values) {
    console.log('submit');
    storeData('user', values, {});
    swipper.scrollBy(1);
    setSwipeAllowed(true);
  }

  function enviar(values) {
    storeData('user', values, {});
  }

  function getFoto() {
    return AsyncStorage.getItem('foto', (err, result) => {
      if (result !== null) {
        const img = JSON.parse(result);
        if (img !== null) {
          setFoto(img.foto);
        }
      }
    });
  }

  async function createUserObj(obj) {
    //console.log('createUserObj');

    let us = JSON.parse(obj);
    let foto = await getItemStored('foto');

    let picture = JSON.parse(foto);

    us.image = picture.foto.base64;
    us.image_url = picture.foto.uri;
    us.login = us.email;

    if (Object.keys(address).length > 0) {
      us.city = address.localidade;
      us.street = address.logradouro;
      us.district = address.bairro;
      us.state = address.state;
    }
    us.birthdate = new Date(us.birthdate);

    us.sel_groups_3_4 = 3;
    us.sel_groups_1 = 1;
    us.sel_groups_135_136 = 135;
    us.in_group_10 = true;

    us.password = us.password;
    us.new_password = us.password;

    return us;
  }

  function createUser(user) {
    console.log('createUser');
    setLoaderVisible(true);

    storeData('user', user, {}).then(async (res) => {
      let user_obj = await createUserObj(res);

      const odoo = getOdoo('admin', '1ND1C0p4c1f1c0');

      odoo.connect().then(function (res) {
        console.log(res);
        odoo.create('res.users', user_obj)
          .then(res => {
            console.log(res);
            if (res.success) {
              storeData('user', user_obj);
              sendLogin(odoo, user_obj.email);
            } else {
              setLoaderVisible(false);
              throw res;
            }
          })
          .catch(e => {
            setMessage(e.error.data.arguments[0]);
            setOverlayVisible(true);
          })
      })
    });
  }

  async function sendLogin(odoo, email) {
    const params = {
      domain: [['email', '=', email]],
      fields: [
        'name',
        'image',
        'birthdate',
        'function',
        'cnpj_cpf',
        'education_level',
        'gender',
        'missions_count',
        'mobile',
        'email',
        'street',
        'city',
        'district',
        'state',
        'signup_url',
      ]
    }

    odoo.search_read('res.partner', params)
      .then(response => {
        console.log(response);
        if (response.success) {
          const u = response.data[0];
          setUser(u);
          setLoaderVisible(false);
          // setConfirmEmail(true);
          props.navigation.navigate('Browser', { url: u.signup_url });
        } else {
          setLoaderVisible(false);
          Alert.alert('Erro de conexão.');
        }
      })
      .catch(e => {
        console.log('erro search_read ');
        console.log(e);
      });
  }

  async function getCameraPermission() {
    const { status } = await Camera.requestPermissionsAsync();
    setHasPermission(status === 'granted');
    if (status === 'granted') {
      props.navigation.navigate('Foto');
    }
  }

  useFocusEffect(() => {
    getFoto();
  }, []);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    setLoaderVisible(true);

    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    };
  }, []);

  const _keyboardDidShow = () => {
    // if(swipeAllowed){
    setShowPagination(false);
    // }
  };

  const _keyboardDidHide = () => {
    Keyboard.dismiss();
    // if(swipeAllowed){
    setShowPagination(true);
    // }
  };

  const onBackPress = () => {
    Keyboard.dismiss();
    props.navigation.navigate('Login');
    return false;
  };

  const _onNavigationStateChange = (webViewState) => {
    if(webViewState.url == "https://charismabi.com.br/my"){
      props.navigation.navigate('Main');
    }
  }

  return (
    <SafeAreaView style={[styles.white, { flexGrow: 1 }]}>
      <View style={[styles.titleView, styles.white, { alignContent: 'center' }]}>
        <View style={{ flexGrow: 1 }}>
          <Text style={[styles.title, { textAlign: 'center' }]}>Cadastro</Text>
        </View>
        <View style={{ position: 'absolute', top: 8, left: 0 }}>
          <Icon
            name='keyboard-arrow-left'
            size={36}
            color='#333'
            onPress={() => {
              console.log('click back login');
              props.navigation.navigate('Login');
            }}
          />
        </View>
      </View>

      {loaderVisible &&
        <AnimatedLoader
          visible={true}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader/loader.json')}
          animationStyle={styles.lottie}
          speed={1}
        />
      }

      <WebView
        source={{ uri: 'http://charismabi.com.br/web/signup' }}
        onLoad={loaderVisible => {
          console.log(loaderVisible)
          setLoaderVisible(false)
        }}
        onNavigationStateChange={_onNavigationStateChange.bind(this)}
      />

    </SafeAreaView>
  );
}
