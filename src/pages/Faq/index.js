import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import Constants from 'expo-constants';
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Header } from 'react-native-elements';

const BACON_IPSUM =
  "Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs. \
  Picanha beef prosciutto meatball turkey shoulder shank salami cupim doner jowl pork belly cow. \
  Chicken shankle rump swine tail frankfurter meatloaf ground round flank ham \
  hock tongue shank andouille boudin brisket.";

const CONTENT = [
  {
    title: 'First Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Second my Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Third Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Fourth the Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Fifth Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Sixth Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Seventh my Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Eighth Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Nineth the Question?',
    content: BACON_IPSUM,
  },
  {
    title: 'Tenth Question?',
    content: BACON_IPSUM,
  },
];

import styles from './styles.js';

export default function Faq(props) {
  const [state, setState] = useState({
    activeSections: [],
    collapsed: true,
    multipleSelect: false,
  });

  function toggleExpanded(){
    setState({ collapsed: !state.collapsed });
  };

  const setSections = sections => {
    setState({
      activeSections: sections.includes(undefined) ? [] : sections,
    });
  };

  const renderHeader = (section, _, isActive) => {
    return (
      <Animatable.View
        duration={400}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <Text style={[styles.headerText, 
          isActive ? styles.headerActive : styles.headerInactive]}>{section.title}</Text>
      </Animatable.View>
    );
  };

  function renderContent(section, _, isActive) {
    return (
      <Animatable.View
        duration={400}
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <Animatable.Text animation={isActive ? 'bounceIn' : undefined}>
          <Text style={{lineHeight: 25}}>{section.content}</Text>
        </Animatable.Text>
      </Animatable.View>
    );
  }

  const { activeSections } = state;

  return (
    <SafeAreaView style={[styles.container]} >
      <Header
        backgroundColor='#fff'
        leftComponent={{ 
          icon: 'menu', 
          size: 32,
          color: '#333',
          onPress: () => props.navigation.openDrawer()
        }}
        centerComponent={{
          text: 'FAQ',
          style: { 
            color: '#333',
            fontSize: 22
          }
        }}
        containerStyle={{
          borderBottomWidth: 0, 
          paddingTop: 0, 
          height: 60
        }}
      />
      <View style={{ backgroundColor: '#fff'}}>
        <Accordion
          activeSections={activeSections}
          sections={CONTENT}
          touchableComponent={TouchableHighlight}
          expandMultiple={false}
          renderHeader={renderHeader}
          renderContent={renderContent}
          duration={400}
          onChange={setSections}
          sectionContainerStyle={{borderBottomWidth: 1, borderTopWidth: 1}}
          containerStyle={{borderBottomWidth: 1, borderTopWidth: 1}}
        />
      </View>
    </SafeAreaView>
  )
  
}