import React, {useState, useRef} from 'react';
import {
  AsyncStorage,
  View
} 
from 'react-native';
import { WebView } from 'react-native-webview';
import AnimatedLoader from "react-native-animated-loader";
import { useFocusEffect } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {Header} from 'react-native-elements';

export default function Browser({ route, navigation }) {
  const [confirmUrl, setConfirmUrl] = useState('');

  const webViewRef = useRef();

  function getUrl(){
    const {url} = route.params;
    setConfirmUrl(url);
    webViewRef.current.reload();
  }

  useFocusEffect(() => {
    getUrl();
  }, []);
  
  const _onNavigationStateChange = ({url}) => {
    console.log(url != 'about:blank');
    console.log(url);
    console.log(url != confirmUrl && confirmUrl != '');
    if (url != confirmUrl && confirmUrl != '' && url != 'about:blank') {
      webViewRef.current.stopLoading();
      navigation.navigate('Login');
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Header
        backgroundColor='#e40063'
        leftComponent={{ 
          icon: 'arrow-back', 
          size: 32,
          color: '#fff',
          onPress: () => props.navigation.navigate('Login')
        }}
        containerStyle={{
          borderBottomWidth: 0, 
          paddingTop: 0, 
          height: 60
        }}
      />
      <WebView 
        source={{ uri: confirmUrl }} 
        style={{ 
          marginTop: -100, 
          height: '100%',
          width: '100%' }}
        ref={(ref) => webViewRef.current = ref}
        onLoad={
          e => {
            // Update the state so url changes could be detected by React and we could load the mainUrl.
            console.log(e.nativeEvent.url)
          }
        }
        onNavigationStateChange={_onNavigationStateChange}
      />
    </SafeAreaView>
    
  )
}
